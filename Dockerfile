FROM alpine:3.6

RUN set -ex \
	&& apk add --no-cache --update libsodium py2-pip \
	&& pip --no-cache-dir install https://github.com/gitugser/dumpls/archive/master.zip

ENTRYPOINT ["/usr/bin/dsserver"]